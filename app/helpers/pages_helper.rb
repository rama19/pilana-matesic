module PagesHelper

  def main_menu_items
    [
      ["Home", root_path],
      ["O nama", about_path],
      ["Usluge", usluge_path],
      ["Kontakt", kontakt_path]
    ]
  end

  def render_main_menu
    items = main_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      content_tag(:li, link_to(item[0], item[1]), class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "nav-list")
  end
end
