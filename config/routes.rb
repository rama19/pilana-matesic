Rails.application.routes.draw do

  root to: "pages#home"

  %w(
    about
    usluge
    kontakt
  ).each do |page|
    get page, to: "pages##{page}", as: page
  end
end
